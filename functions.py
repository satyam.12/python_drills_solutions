def is_prime(n):
    """
    Check whether a number is prime or not
    """
    flag = True
    for i in range(2,(n//2)+1):
        if n % i == 0:
            flag = False
    return flag


def n_digit_primes(digit=2):
    """
    Return a list of `n_digit` primes using the `is_prime` function.

    Set the default value of the `digit` argument to 2
    """
    pr_list,range_end,range_start = [],{1:10,2:100,3:1000,4:10000,5:100000},{1:2,2:10,3:100,4:1000,5:10000}
    end,start = range_end[digit],range_start[digit]
    for i in range(start,end):
        check = is_prime(i)
        if check:
            pr_list.append(i)
    return pr_list